@echo off

Setlocal EnableDelayedExpansion

set /a summator = 1

if "%1" == "" (
echo "Parameter isn't here."
goto :EOF
)

if exist %1 (

for %%t in (%1) do (
echo Step: %%t
rename %%t !summator!%~x1
set /a summator = !summator! + 1
)

) else (
echo "That files aren't here."
)

endlocal