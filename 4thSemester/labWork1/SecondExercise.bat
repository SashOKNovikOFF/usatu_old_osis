@echo off

REM If chose folder doesn't exist
if not exist "%2%" (
echo "This folder does not exist here."
goto :EOF
)

REM If chose files doesn't exist
if not exist "%2\*%~x1" (
echo "There is no chose files to copy."
goto :EOF
)

REM Create folder, where will be copy chose files
if not exist "CopiedFiles\" mkdir "CopiedFiles"

REM Copy chose files from chose folder in "CopiedFiles" folder
xcopy "%2\*%~x1" "CopiedFiles\"