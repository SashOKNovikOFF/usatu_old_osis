@echo off

if exist "Human" tree "%cd%\human"

if not exist "Human" mkdir "Human"
cd "Human"

if not exist "Mind" mkdir "Mind"
if not exist "Body" mkdir "Body"
if not exist "Friend" mkdir "Friend"

cd "Mind"
if not exist "Knowledge" mkdir "Knowledge"
if not exist "Willing" mkdir "Willing"
if not exist "Needs" mkdir "Needs"
cd ..

cd "Body"
if not exist "Dress" mkdir "Dress"
if not exist "Food" mkdir "Food"
if not exist "Boxes" mkdir "Boxes"
cd ..

cd "Friend"
if not exist "Girls" mkdir "Girls"
if not exist "Boys" mkdir "Boys"
if not exist "Aliens" mkdir "Aliens"
cd ..
cd ..

tree "%cd%\human"