#!/bin/bash

# Проверка на наличие трёх аргументов, содержащих
# названия созданных пользователем файлов 
if [ $# != 3 ]; then
    echo "Invalid arguments."
    exit
fi
# Проверка на наличие созданных файлов
if [ ! -e $1 ]; then
    echo "There is no empty file."
    exit
fi
if [ ! -e $2 ]; then
    echo "There is no .lib file."
    exit
fi
if [ ! -e $3 ]; then
    echo "There is no .doc file."
    exit
fi

# Переместить файл emptyFile в папку /usr/bin
sudo mv $1 /usr/bin
# Изменить группу файла emptyFile на группу users
sudo chgrp users /usr/bin/$1
# Добавить права на выполнение группе users файла emptyFile
sudo chmod g+x /usr/bin/$1

# Переместить файл file.lib в папку /usr/lib
sudo mv $2 /usr/lib

# Создать папку /usr/share/doc/novikov
sudo mkdir /usr/share/doc/novikov
# Переместить файл file.doc в папку /usr/share/doc/novikov
sudo mv $3 /usr/share/doc/novikov