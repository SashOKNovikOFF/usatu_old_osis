#!/bin/bash

# Если были введены два параметра командной строки:
# имя каталога и размер файлов в мегабайтах
if [ $# == 2 ]; then
    directory=$1
    number=$2
# Если не были введены параметры, пользователь вводит
# имя каталога и размер файлов в мегабайтах
elif [ $# == 0 ]; then
    # echo -n Не выводить в конце символ новой строки
    echo -n "Enter the path of your directory: "
    read directory
    echo -n "Enter the size of needed files: "
    read number
# Сообщить о неверно введённых параметрах командной строки
# и закончить работу программы
else
    echo "Uncorrect arguments."
    exit
fi

# Если каталог не существует, сообщить пользователю об этом
# и закончить работу программы
if [ ! -d $directory ]; then
    echo "There is no chosen directory."
    exit
fi

# Переменная, в которой хранится параметр для команды find 
tempSize='+'$number'M'

# Создать log-файл, куда будут записываться
# отчёт о выполненных скриптом действиях 
logFile=Novikov.log
sudo touch $logFile

# Записать в созданный log-файл дату выполнения скрипта,
# команду, выполняющую поиск файлов, и список найденных файлов
date > $logFile
echo "find $directory -size $tempSize" >> $logFile
find –type f $directory -size $tempSize >> $logFile

# В переменную tempString запомнить список найденных файлов 
tempString=$(find $directory -size $tempSize)
# В цикле пройтись по всем найденным файлам,
# для каждого файла записать в Log-файл
# время выполнения команды и название команды
for i in $tempString; do
    # Архивировать файл, не удаляя его
    gzip $i -k
    date >> $logFile
    echo "gzip $i -k" >> $logFile
    
    # Удалить файл
    rm $i
    date >> $logFile
    echo "rm $i" >> $logFile
    
    # Создать пустой файл с таким же именем
    touch ${i%%.*}
    date >> $logFile
    echo "touch ${i%%.*}" >> $logFile
done

# Перемещаем log-файл в папку /var/log/
sudo mv $logFile /var/log/