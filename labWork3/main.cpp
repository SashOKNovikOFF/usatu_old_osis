// C++ standard includes
#include <iostream>

// User includes
#include "mathFunctions.h"

mathFunctions object;

int main()
{
    int numberA, numberB;
        
    std::cout << "Enter two numbers [m, n]: ";
    std::cin  >> numberA >> numberB;

    if (std::cin.fail())
    {
        std::cerr << "Error. Inccorect data." << std::endl;
        return 1;
    };

    if ((numberA == 0) && (numberB == 0))
    {
       std::cout << "All numbers are zero." << numberA << " " << numberB;
       std::cout << std::endl;
    }    
    else
    {
       std::cout << "Difference between LCM and GCD is ";
       std::cout << object.result(numberA, numberB) << ".";
       std::cout << std::endl;
    };

    return 0;
}
