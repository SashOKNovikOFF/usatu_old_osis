// C++ standard includes
#include <cmath>

// User includes
#include "mathFunctions.h"

int mathFunctions::LCM(int numberA, int numberB)
{
    return numberA / GCD(numberA, numberB) * numberB;
};

int mathFunctions::GCD(int numberA, int numberB)
{
    while((numberA > 0) && (numberB > 0))
    {
        if (numberA > numberB)
            numberA %= numberB;
        else
            numberB %= numberA;
    };

    return numberA + numberB;
};

int mathFunctions::result(int numberA, int numberB)
{
    return std::abs(LCM(numberA, numberB) - GCD(numberA, numberB));
};
