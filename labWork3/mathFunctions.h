#ifndef MATHFUNCTIONS_H
#define MATHFUNCTIONS_H

class mathFunctions
{
private:
    int LCM (int numberA, int numberB);   //!< Наименьшее общее кратное (НОК)
    int GCD (int numberA, int numberB);   //!< Наибольший общий делитель (НОД)
public:
    int result(int numberA, int numberB); //!< Разность НОК и НОД
};

#endif // MATHFUNCTIONS_H
