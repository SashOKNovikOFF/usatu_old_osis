// C standard includes
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> // for close()
#include <string.h>

// Socket API includes
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

// User headers
#include "netFunctions.h"

int main(int argc, char* argv[])
{
    int clientSocket;
    struct sockaddr_in serverAddress;
    
    int portNumber;
    if(enterPortNumber(argc, argv, &portNumber) < 0)
        exit(1);

    clientSocket = socket(AF_INET, SOCK_STREAM, 0);
    if(clientSocket < 0)
    {
        perror("Error with socket creating");
        exit(2);
    };

    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = htons(portNumber);
    serverAddress.sin_addr.s_addr = htonl(INADDR_LOOPBACK);

    if(connect(clientSocket, (struct sockaddr*) &serverAddress, sizeof(serverAddress)) < 0)
    {
        perror("Error with connect");
        exit(3);
    };

    char buffer[1024];
    char login[64];
    char password[64];
    
    // Send login
    printf("Welcome to server program. Enter your name: ");
    scanf("%s", login);
    send(clientSocket, login, sizeof(login), 0);
    
    // Send password
    printf("Enter your password: ");
    scanf("%s", password);
    send(clientSocket, password, sizeof(password), 0);

    // Authorization check
    recv(clientSocket, buffer, sizeof(buffer), 0);
    if (strcmp(buffer, "-1") == 0)
    {
        printf("Error: login (or password) is incorrect.\n");
        exit(4);
    }
    else
        printf("%s\n", buffer);

    // Input data
    int numberA, numberB;
    char number1st[10], number2nd[10];

    printf("Enter two numbers [m, n]: ");
    if (scanf("%i %i", &numberA, &numberB) != 2)
    {
        printf("Error: numbers are incorrect.\n");
        exit(5);
    }
    else if ((numberA == 0) && (numberB == 0))
    {
       printf("Error: all numbers are zero.\n");
       exit(6);
    };
    
    // Send numbers
    sprintf(number1st, "%i", numberA);
    sprintf(number2nd, "%i", numberB);

    send(clientSocket, number1st, sizeof(number1st), 0);
    send(clientSocket, number2nd, sizeof(number2nd), 0);

    // Get result
    recv(clientSocket, buffer, sizeof(buffer), 0);
    printf("Difference between LCM and GCD is %s.\n", buffer);

    close(clientSocket);

    return 0;
};
