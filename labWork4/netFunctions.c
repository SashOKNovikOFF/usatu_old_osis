// Related headers
#include "netFunctions.h"

// C standard includes
#include <stdio.h>
#include <stdlib.h>

int enterPortNumber(int argc, char* argv[], int* portNumber)
{
    int leftBorderPort = 49152;
    int rightBorderPort = 65535;
    int defaultPort = 54545;
    
    if (argc == 1)
    {
        *portNumber = defaultPort;
        return 0;
    }
    else if (argc == 2)
    {
        *portNumber = atoi(argv[1]);
        if ((*portNumber < leftBorderPort) || (*portNumber > rightBorderPort))
        {
            printf("Error: ports number must be between %i and %i.\n", leftBorderPort, rightBorderPort);
            return -1;
        };
    }
    else
    {
        printf("Error: invalid arguments.\n");
        return -1;
    };

    return 0;
};
