// C standard includes
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> // for close()
#include <string.h>

// Socket API includes
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

// User includes
#include "netFunctions.h"
#include "mathFunctions.h"

int main(int argc, char* argv[])
{
    int acceptSocket, listenerSocket;
    struct sockaddr_in localAddress;
    
    int portNumber;
    if(enterPortNumber(argc, argv, &portNumber) < 0)
        exit(1);
    
    listenerSocket = socket(AF_INET, SOCK_STREAM, 0);
    if(listenerSocket < 0)
    {
        perror("Error with socket creating");
        exit(2);
    }
    
    localAddress.sin_family = AF_INET;
    localAddress.sin_port = htons(portNumber);
    localAddress.sin_addr.s_addr = htonl(INADDR_ANY);

    if(bind(listenerSocket, (struct sockaddr*) &localAddress, sizeof(localAddress)) < 0)
    {
        perror("Error with socket binding");
        exit(3);
    }

    listen(listenerSocket, 1);

    char login[64], password[64];
    char defaultLogin[] = "admin";
    char defaultPassword[] = "admin";

    while(1)
    {
        acceptSocket = accept(listenerSocket, NULL, NULL);
        if(acceptSocket < 0)
        {
            perror("Error with accepting");
            continue;
        };
        
        char buffer[1024];
        
        // Request login and password
        recv(acceptSocket, login, sizeof(login), 0);
        recv(acceptSocket, password, sizeof(password), 0);

        // Authorization check
        if ((strcmp(login, defaultLogin) != 0) || (strcmp(password, defaultPassword) != 0))
        {
            strcpy(buffer, "-1");
            send(acceptSocket, buffer, sizeof(buffer), 0);

            close(acceptSocket);
            continue;
        }
        else
        {
            strcpy(buffer, "Authorization is correct.");
            send(acceptSocket, buffer, sizeof(buffer), 0);
        };

        char number1st[10];
        char number2nd[10];

        // Request data
        recv(acceptSocket, number1st, sizeof(number1st), 0);
        recv(acceptSocket, number2nd, sizeof(number2nd), 0);

        // Send result
        int result = LGresult(atoi(number1st), atoi(number2nd));
        sprintf(buffer, "%i", result);
        send(acceptSocket, buffer, sizeof(buffer), 0);

        close(acceptSocket);
    }
    
    return 0;
};
