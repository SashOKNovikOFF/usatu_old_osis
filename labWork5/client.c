// C standard includes
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> // for close()
#include <string.h>

// Socket API includes
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

// User headers
#include "clientFunctions.h"

int main(int argc, char* argv[])
{
    int clientSocket;
    struct sockaddr_in serverAddress;
    
    int portNumber;
    if(enterClientArgs(argc, argv, &portNumber) < 0)
        exit(1);

    clientSocket = socket(AF_INET, SOCK_STREAM, 0);
    if(clientSocket < 0)
    {
        perror("Error with socket creating");
        exit(2);
    };

    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = htons(portNumber);
    serverAddress.sin_addr.s_addr = htonl(INADDR_LOOPBACK);

    if(connect(clientSocket, (struct sockaddr*) &serverAddress, sizeof(serverAddress)) < 0)
    {
        perror("Error with connect");
        exit(3);
    };

    char buffer[1024];
    char login[64];
    char password[64];
    
    // Send login
    printf("Welcome to server program. Enter your name: ");
    scanf("%s", login);
    send(clientSocket, login, sizeof(login), 0);
    
    // Send password
    printf("Enter your password: ");
    scanf("%s", password);
    send(clientSocket, password, sizeof(password), 0);

    // Authorization check
    recv(clientSocket, buffer, sizeof(buffer), 0);
    if (strcmp(buffer, "-1") == 0)
    {
        printf("Error: login (or password) is incorrect.\n");
        exit(4);
    }
    else
        printf("%s\n", buffer);
        
    callMainMenu(clientSocket, buffer, sizeof(buffer), 0);

    close(clientSocket);

    return 0;
};
