// Related headers
#include "clientFunctions.h"

// C standard includes
#include <stdio.h>
#include <stdlib.h> // for atoi()
#include <stdio_ext.h> // for __fpurge()
#include <string.h>

// Socket API includes
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

int enterClientArgs(int argc, char* argv[], int* portNumber)
{
    int leftBorderPort = 49152;
    int rightBorderPort = 65535;
    int defaultPort = 54545;
    
    if (argc == 1)
    {
        *portNumber = defaultPort;
        return 0;
    }
    else if (argc == 2)
    {
        *portNumber = atoi(argv[1]);
        if ((*portNumber < leftBorderPort) || (*portNumber > rightBorderPort))
        {
            printf("Error: ports number must be between %i and %i.\n", leftBorderPort, rightBorderPort);
            return -1;
        };
    }
    else
    {
        printf("Error: invalid arguments.\n");
        return -1;
    };

    return 0;
};

void callMainMenu(int clientSocket, char* buffer, int length, int flags)
{
    int exitFlag = 1;
    while(exitFlag)
    {
        printf("MAIN MENU\n");
        printf("1 - get a list of variables;\n");
        printf("2 - enter values of variables;\n");
        printf("3 - get result;\n");
        printf("4 - reset all variables;\n");
        printf("5 - quit from server.\n");
    
        int menuItem;
        printf("Choose one of the menu item [1-5]: ");
        if (scanf("%i", &menuItem) != 1)
        {
            printf("Error: incorrect input.\n");
            
            __fpurge(stdin);
            continue;
        };
        switch(menuItem)
        {
            case 1:  { callClientVAR(clientSocket, buffer, length, flags);                break; }
            case 2:  { callClientDATA(clientSocket, buffer, length, flags);               break; }
            case 3:  { callClientRES(clientSocket, buffer, length, flags);                break; }
            case 4:  { callClientRSET(clientSocket, buffer, length, flags);               break; }
            case 5:  { callClientQUIT(clientSocket, buffer, length, flags); exitFlag = 0; break; }
            default: { printf("Error: incorrect input.\n"); continue;                     break; }
        };
        
        printf("%s\n", buffer);
    };
};

void callClientVAR(int clientSocket, char* buffer, int length, int flags)
{
    sprintf(buffer, "VAR");
    send(clientSocket, buffer, length, flags);
    recv(clientSocket, buffer, length, flags);
};
void callClientDATA(int clientSocket, char* buffer, int length, int flags)
{
    int numberA, numberB;
    char temp[512];

    while(1)
    {
        printf("Enter two numbers [m, n]: ");

        if (scanf("%i %i", &numberA, &numberB) != 2)
            printf("Error: numbers are incorrect. Try again.\n");
        else
            break;

        __fpurge(stdin);
    };
    
    strcpy(buffer, "DATA ");

    strcat(buffer, "firstNumber=");
    sprintf(temp, "%i", numberA);
    strcat(buffer, temp);

    strcat(buffer, ",");

    strcat(buffer, "secondNumber=");
    sprintf(temp, "%i", numberB);
    strcat(buffer, temp);

    send(clientSocket, buffer, length, flags);
    recv(clientSocket, buffer, length, flags);
};
void callClientRES(int clientSocket, char* buffer, int length, int flags)
{
    sprintf(buffer, "RES");
    send(clientSocket, buffer, length, flags);
    recv(clientSocket, buffer, length, flags);
};
void callClientRSET(int clientSocket, char* buffer, int length, int flags)
{
    sprintf(buffer, "RSET");
    send(clientSocket, buffer, length, flags);
    recv(clientSocket, buffer, length, flags);
};
void callClientQUIT(int clientSocket, char* buffer, int length, int flags)
{
    sprintf(buffer, "QUIT");
    send(clientSocket, buffer, length, flags);
    recv(clientSocket, buffer, length, flags);
};
