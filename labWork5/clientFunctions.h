#ifndef CLIENTFUNCTIONS_H
#define CLIENTFUNCTIONS_H

int enterClientArgs(int argc, char* argv[], int* portNumber);

void callMainMenu(int clientSocket, char* buffer, int length, int flags);

void callClientVAR(int clientSocket, char* buffer, int length, int flags);
void callClientDATA(int clientSocket, char* buffer, int length, int flags);
void callClientRES(int clientSocket, char* buffer, int length, int flags);
void callClientRSET(int clientSocket, char* buffer, int length, int flags);
void callClientQUIT(int clientSocket, char* buffer, int length, int flags);

#endif // CLIENTFUNCTIONS_H
