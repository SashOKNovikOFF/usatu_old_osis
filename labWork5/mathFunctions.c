// C++ standard includes
#include <math.h>
#include <stdlib.h> // for abs()

// User includes
#include "mathFunctions.h"

int LCM(int numberA, int numberB)
{
    return numberA * numberB / GCD(numberA, numberB);
};

int GCD(int numberA, int numberB)
{
    while((numberA > 0) && (numberB > 0))
    {
        if (numberA > numberB)
            numberA %= numberB;
        else
            numberB %= numberA;
    };

    return numberA + numberB;
};

int LGresult(int numberA, int numberB)
{
    return abs(LCM(numberA, numberB) - GCD(numberA, numberB));
};
