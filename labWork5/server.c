// C standard includes
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> // for close()
#include <string.h>

// Socket API includes
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

// User includes
#include "serverFunctions.h"

int main(int argc, char* argv[])
{
    int acceptSocket, listenerSocket;
    struct sockaddr_in localAddress;
    
    int portNumber, connectNumber;
    if(enterServerArgs(argc, argv, &portNumber, &connectNumber) < 0)
        exit(1);
    
    listenerSocket = socket(AF_INET, SOCK_STREAM, 0);
    if(listenerSocket < 0)
    {
        perror("Error with socket creating");
        exit(2);
    };
    
    localAddress.sin_family = AF_INET;
    localAddress.sin_port = htons(portNumber);
    localAddress.sin_addr.s_addr = htonl(INADDR_ANY);

    if(bind(listenerSocket, (struct sockaddr*) &localAddress, sizeof(localAddress)) < 0)
    {
        perror("Error with socket binding");
        exit(3);
    };

    listen(listenerSocket, connectNumber);

    char login[64], password[64];
    char defaultLogin[] = "admin";
    char defaultPassword[] = "admin";

    while(1)
    {
        acceptSocket = accept(listenerSocket, NULL, NULL);
        if(acceptSocket < 0)
        {
            perror("Error with accepting");
            continue;
        };
        
        char buffer[1024];
        
        // Request login and password
        recv(acceptSocket, login, sizeof(login), 0);
        recv(acceptSocket, password, sizeof(password), 0);

        // Authorization check
        if ((strcmp(login, defaultLogin) != 0) || (strcmp(password, defaultPassword) != 0))
        {
            strcpy(buffer, "-1");
            send(acceptSocket, buffer, sizeof(buffer), 0);

            close(acceptSocket);
            continue;
        }
        else
        {
            strcpy(buffer, "250 ALEX_SERVER 0.1");
            send(acceptSocket, buffer, sizeof(buffer), 0);
        };
        
        const int numberCount = 2;
        int numbers[numberCount];
        int inputFlag = 0;
        int leftBorder = 1;
        int rightBorder = 1000;
        
        while(1)
        {
            recv(acceptSocket, buffer, sizeof(buffer), 0);
            
            char tokens[1024];
            strcpy(tokens, buffer);
            char* menuItem = strtok(tokens, " ");
            
            if (strcmp(menuItem, "VAR") == 0)
            {
                callServerVAR(buffer, numberCount, leftBorder, rightBorder);
                send(acceptSocket, buffer, sizeof(buffer), 0);
            }
            else if (strcmp(menuItem, "DATA") == 0)
            {
                callServerDATA(buffer, &inputFlag, numbers, numberCount, leftBorder, rightBorder);
                send(acceptSocket, buffer, sizeof(buffer), 0);
            }
            else if (strcmp(menuItem, "RES") == 0)
            {
                callServerRES(buffer, &inputFlag, numbers);
                send(acceptSocket, buffer, sizeof(buffer), 0);
            }
            else if (strcmp(menuItem, "RSET") == 0)
            {
                callServerRSET(buffer, &inputFlag, numbers, numberCount);
                send(acceptSocket, buffer, sizeof(buffer), 0);
            }
            else if (strcmp(menuItem, "QUIT") == 0)
            {
                callServerQUIT(buffer);
                send(acceptSocket, buffer, sizeof(buffer), 0);
                
                break;
            };
        };
        
        close(acceptSocket);
    };
    
    return 0;
};
