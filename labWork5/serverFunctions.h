#ifndef SERVERFUNCTIONS_H
#define SERVERFUNCTIONS_H

int enterServerArgs(int argc, char* argv[], int* portNumber, int* connectNumber);

void callServerVAR(char* buffer, int numberCount, int leftBorder, int rightBorder);
void callServerDATA(char* buffer, int* inputFlag, int* numbers, int numberCount, int leftBorder, int rightBorder);
void callServerRES(char* buffer, int* inputFlag, int* numbers);
void callServerRSET(char* buffer, int* inputFlag, int* numbers, int numberCount);
void callServerQUIT(char* buffer);

#endif // SERVERFUNCTIONS_H
