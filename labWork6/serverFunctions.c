// Related headers
#include "serverFunctions.h"
#include "mathFunctions.h"

// C standard includes
#include <stdio.h>
#include <stdlib.h> // for atoi()
#include <string.h>

int enterServerArgs(int argc, char* argv[], int* portNumber, int* connectNumber)
{
    int leftBorderPort = 49152;
    int rightBorderPort = 65535;
    int defaultPort = 54545;
    
    int leftBorderConnect = 1;
    int rightBorderConnect = 10;
    int defaultConnect = 1;
    
    if (argc == 1)
    {
        *portNumber = defaultPort;
        *connectNumber = defaultConnect;
        return 0;
    }
    else if (argc == 2)
    {
        *portNumber = atoi(argv[1]);
        *connectNumber = defaultConnect;
        if ((*portNumber < leftBorderPort) || (*portNumber > rightBorderPort))
        {
            printf("Error: ports number must be between %i and %i.\n", leftBorderPort, rightBorderPort);
            return -1;
        };
    }
    else if (argc == 3)
    {
        *portNumber = atoi(argv[1]);
        if ((*portNumber < leftBorderPort) || (*portNumber > rightBorderPort))
        {
            printf("Error: ports number must be between %i and %i.\n", leftBorderPort, rightBorderPort);
            return -1;
        };
        
        *connectNumber = atoi(argv[2]);
        if ((*connectNumber < leftBorderConnect) || (*connectNumber > rightBorderConnect))
        {
            printf("Error: connect number must be between %i and %i.\n", leftBorderConnect, rightBorderConnect);
            return -1;
        };
    }
    else
    {
        printf("Error: invalid arguments.\n");
        return -1;
    };

    return 0;
};

void callServerVAR(char* buffer, int numberCount, int leftBorder, int rightBorder)
{
    char temp[512];

    strcpy(buffer, "220 VARIABLES\n");

    for (int i = 0; i < numberCount; i++)
    {
        sprintf(temp, "number%i\n", i);
        strcat(buffer, temp);
        
        sprintf(temp, "%i ", leftBorder);       
        strcat(buffer, temp);
        sprintf(temp, "%i\n", rightBorder);
        strcat(buffer, temp);
    };

    strcat(buffer, "250 VARIABLES");
};
void callServerDATA(char* buffer, int* inputFlag, int* numbers, int numberCount, int leftBorder, int rightBorder)
{
    if (*inputFlag == 1)
    {
        strcpy(buffer, "510 VARIABLES ALREADY ENTERED");
        return;
    };
    
    char* tokens = strtok(buffer, " "); // get DATA command
    
    for (int i = 0; i < numberCount; i++)
    {
        // get variable name
        tokens = strtok(NULL, "=");
        if (tokens == NULL)
        {
            printf("%s", tokens);
            strcpy(buffer, "530 NOT ENOUGH VARIABLES");
            return;
        };
        
        // get number
        tokens = strtok(NULL, ",");
        int temp = atoi(tokens);
        if ((temp != 0) && (temp >= leftBorder) && (temp <= rightBorder))
            numbers[i] = temp;
        else
        {
            strcpy(buffer, "520 OUT OF RANGE");
            return;
        };
    };
    
    if (strtok(NULL, " ,=") == NULL)
    {
        strcpy(buffer, "250 SUCCESS INPUT");
        *inputFlag = 1;
    }
    else
        strcpy(buffer, "540 TOO MUCH VARIABLES");
};
void callServerRES(char* buffer, int* inputFlag, int* numbers)
{
    if (*inputFlag == 0)
    {
        strcpy(buffer, "540 NUMBER ARE NOT ENTERED");
        return;
    };
    
    int temp = LGresult(numbers[0], numbers[1]);
    if (temp < 0)
    {
       sprintf(buffer, "550 DIFFERENCE BETWEEN LCM AND GCD < 0");
       return;
    };
    
    sprintf(buffer, "220 DIFFERENCE BETWEEN LCM AND GCD: %i", temp);
};
void callServerRSET(char* buffer, int* inputFlag, int* numbers, int numberCount)
{
    *inputFlag = 0;
    for (int i = 0; i < numberCount; i++)
        numbers[i] = -1;
    
    sprintf(buffer, "250 RESET ALL VARIABLES");
};
void callServerQUIT(char* buffer)
{
    sprintf(buffer, "250 END WORK WITH SERVER");
};
